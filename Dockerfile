FROM golang:1.21 as fixtures
WORKDIR /go/builds
COPY fixtures/ ./
RUN go build -o server server.go

EXPOSE 8090
ENTRYPOINT ["/go/builds/server"]
