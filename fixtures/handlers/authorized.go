package handlers

import (
	"fmt"
	"html/template"
	"net/http"

	"gitlab.com/dast-e2e-multi-step-auth/model"
)

type authorizedData struct {
	Name    string
	Message string
}

type Authorized struct {
	templates *template.Template
	sessions  *model.Sessions
}

func NewAuthorized(sessions *model.Sessions) *Authorized {
	c := Authorized{}
	c.sessions = sessions
	c.templates = template.Must(template.ParseFiles("templates/login.html", "templates/menu.html"))
	return &c
}

func (c *Authorized) Handle(writer http.ResponseWriter, request *http.Request) {
	cookie, _ := request.Cookie("testauth")

	user := c.sessions.Get(cookie)
	if user == nil {
		writer.WriteHeader(http.StatusForbidden)
		if err := c.templates.ExecuteTemplate(writer, "login.html", nil); err != nil {
			fmt.Fprintf(writer, "Failed to execute template :| err: %v", err)
			return
		}
		return
	}

	userData := &authorizedData{Name: user.Username, Message: "welcome user!"}

	if err := c.templates.ExecuteTemplate(writer, "menu.html", userData); err != nil {
		fmt.Fprintf(writer, "Failed to execute template :| err: %v", err)
		return
	}
}
