package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/dast-e2e-multi-step-auth/model"
)

type userLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Login struct {
	sessions *model.Sessions
}

func NewLogin(sessions *model.Sessions) *Login {
	c := Login{}
	c.sessions = sessions
	return &c
}

func (c *Login) Handle(writer http.ResponseWriter, request *http.Request) {
	data, _ := ioutil.ReadAll(request.Body)
	defer func() {
		_ = request.Body.Close()
	}()
	user := &userLogin{}

	if err := json.Unmarshal(data, user); err != nil {
		fmt.Fprintf(writer, `{"error": "invalid login"}`)
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	if user.Email == "test@test.com" && user.Password == "testpass" {
		c.login(writer, user.Email, user.Password)
		return
	}

	fmt.Fprintf(writer, `{"error": "invalid login"}`)
	writer.WriteHeader(http.StatusForbidden)
}

func (c *Login) login(writer http.ResponseWriter, username, password string) {
	cookie := c.sessions.Add(username)
	writer.Header().Add("Set-Cookie", cookie.String())
	fmt.Fprintf(writer, `{"status": "login success", "token": "%s"}`, model.GenerateID())
	writer.WriteHeader(http.StatusOK)
}
