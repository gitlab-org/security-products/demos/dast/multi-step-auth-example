package model

import (
	"crypto/rand"
	"math/big"
	"net/http"
	"sync"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

type Sessions struct {
	ids  map[string]*User
	lock *sync.RWMutex
}

func NewSessions() *Sessions {
	return &Sessions{
		ids:  make(map[string]*User),
		lock: &sync.RWMutex{},
	}
}

func (s *Sessions) Add(username string) http.Cookie {
	s.lock.Lock()
	defer s.lock.Unlock()
	sessionID := GenerateID()
	s.ids[sessionID] = &User{Username: username}
	return http.Cookie{
		Name:     "testauth",
		Value:    sessionID,
		Expires:  time.Now().Add(time.Hour * 72),
		HttpOnly: true,
		SameSite: http.SameSiteDefaultMode,
	}
}

func (s *Sessions) Get(cookie *http.Cookie) *User {
	s.lock.Lock()
	defer s.lock.Unlock()

	if cookie == nil {
		return nil
	}

	user, exist := s.ids[cookie.Value]
	if !exist {
		return nil
	}

	return &User{Username: user.Username}
}

func GenerateID() string {
	const idSize = 64
	id := make([]byte, idSize)
	for i := 0; i < idSize; i++ {
		randIdx, _ := rand.Int(rand.Reader, big.NewInt(int64(len(charset))))
		id[i] = charset[randIdx.Int64()]
	}
	return string(id)
}
