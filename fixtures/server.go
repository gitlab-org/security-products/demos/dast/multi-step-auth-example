package main

import (
	"flag"
	"net/http"

	"gitlab.com/dast-e2e-multi-step-auth/handlers"
	"gitlab.com/dast-e2e-multi-step-auth/model"
	"gitlab.com/dast-e2e-multi-step-auth/webserver"
)

var port int

func init() {
	flag.IntVar(&port, "port", 8090, "port to run web server on")
}

func main() {
	flag.Parse()

	sessions := model.NewSessions()
	login := handlers.NewLogin(sessions)
	authorized := handlers.NewAuthorized(sessions)

	mux := http.NewServeMux()
	fs := http.FileServer(http.Dir("./templates/"))
	mux.Handle("/", fs)
	mux.HandleFunc("/login", login.Handle)
	mux.HandleFunc("/app/login", authorized.Handle)
	mux.HandleFunc("/app/menu", authorized.Handle)
	mux.HandleFunc("/app/a.html", authorized.Handle)
	webserver.New(port, mux).Start()
}
