package webserver

import (
	"io/ioutil"
	"strings"
)

type Template struct {
	FilePath string
	Replace map[string]string
}

func (t *Template) Read() string {
	data, _ := ioutil.ReadFile(t.FilePath)
	output := string(data)

	for template, value := range t.Replace {
		output = strings.ReplaceAll(output, template, value)
	}

	return output
}
