package webserver

import (
	"fmt"
	"net/http"
	"strconv"
)

type WebServer struct {
	port int
	mux  *http.ServeMux
}

func New(port int, mux *http.ServeMux) (s *WebServer) {
	return &WebServer{
		port: port,
		mux:  mux,
	}
}

func (s *WebServer) Start() {
	fmt.Printf("Starting server at port %d\n", s.port)
	if err := http.ListenAndServe(":"+strconv.Itoa(s.port), s.mux); err != nil {
		panic(err)
	}
}
